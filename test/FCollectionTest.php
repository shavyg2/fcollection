<?php
/**
 * Created by JetBrains PhpStorm.
 * User: shavyg2
 * Date: 2/10/14
 * Time: 2:11 PM
 * To change this template use File | Settings | File Templates.
 */

class FCollectionTest extends PHPUnit_Framework_TestCase
{


    public function test_foreach()
    {
        $fake = new FCollection();
        $count = 0;
        foreach ($fake as $f) {
            $count++;
        }

        $this->assertEquals(15, $count);

        $this->assertNotNull($fake[10]);

        $this->assertNotNull($fake->word);

        $this->assertNotNull($fake[10]->name);

        $this->assertEquals(2, count($fake->words(2)));
    }


    public function test_add_methods()
    {
        $fake = new FCollection();
        $fake->add_method("two", function () {
            return 2;
        });

        $fake->add_method("threeplus", function ($num) {
            return 3+$num;
        });

        $this->assertEquals(2, $fake->two());
        $this->assertEquals(6, $fake->threeplus(3));
    }
}
